#include <Arduino.h>

const int pinLED = 13;
const int pinMicro = 9;

void setup() {
  pinMode(pinLED, OUTPUT);
  pinMode(pinMicro, INPUT);
  attachInterrupt(digitalPinToInterrupt(pinMicro),led_encendido,RISING);
}

void loop() {
  // put your main code here, to run repeatedly:
}
void led_encendido  (void){
  digitalWrite(pinLED,HIGH);
}